Your task is to create a web page that will create, edit and index saved bookmarked pages.

What you need to do
1. Use appropriate validation for each field saved.
2. Filter user results by IP address.
3. Create a search form.

What you can do
1. Use front end framework ( jquery, Bootstrap )
2. Use CakePHP as back-end framework ( bonus points )
3. Use any jquery plugin ( negative points :)

Stage your solution on a demo page, fork this repo and create a pull request that contains your implementation in a new branch named after you.